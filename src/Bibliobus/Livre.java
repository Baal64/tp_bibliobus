package Bibliobus;


public class Livre {
	private String titre;
	private String auteur;
	private String editeur;
	private int nbExemplaires;
	private Genre genre;
	private static int nbrLivres;

	public int getNbExemplaires() {
		return nbExemplaires;
	}

	public void setNbExemplaires(int nbExemplaires) {
		while (nbExemplaires <= 0) {
			System.out.println("Entrez un nombre d'exemplaires positif !");
		}
		this.nbExemplaires = nbExemplaires;
	}

	public void nouvelExemplaire() {
		nbExemplaires++;
	}

	public void nouvelExemplaire(int nb) {
		if (nb > 1) {
			nbExemplaires = nbExemplaires + nb;
		}
	}

	public boolean estPresent() {
		if (nbExemplaires > 0) {
			return true;
		}
		return false;
	}

	public void perteExemplaire() {
		if (nbExemplaires > 0) {
			nbExemplaires--;
		}
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public String getTitre() {
		return titre;
	}

	public String getAuteur() {
		return auteur;
	}

	public String getEditeur() {
		return editeur;
	}

	public static int getNbrLivres() {
		return nbrLivres;
	}

	public boolean equals(Livre livre) {

		if(livre.getTitre() == this.getTitre() && livre.getEditeur() == this.getEditeur() && livre.getAuteur() == this.getAuteur()) {
			return true;
		}

		return false;
	}

	public Livre nouvelEditeur(String unEditeur) {
			this.editeur = unEditeur;
			
			Livre livreNew = new Livre(this.titre, this.auteur, unEditeur, this.nbExemplaires, this.genre);
			
		return livreNew;
	}

	public Livre(String titre, String auteur, String editeur, int nbExemplaires, Genre genre) {
		super();
		this.titre = titre;
		this.auteur = auteur;
		this.editeur = editeur;
		this.nbExemplaires = nbExemplaires;
		this.genre = genre;
		nbrLivres++;
	}

	public Livre(String titre, String auteur, String editeur, int nbExemplaires) {
		super();
		this.titre = titre;
		this.auteur = auteur;
		this.editeur = editeur;
		this.nbExemplaires = nbExemplaires;
		this.genre = Genre.NON_SPECIFIE;
		nbrLivres++;
	}

	public Livre() {
		super();
		nbrLivres++;
	}

	public Livre(String titre, String auteur, String editeur) {
		super();
		this.titre = titre;
		this.auteur = auteur;
		this.editeur = editeur;
		this.nbExemplaires = 0;
		this.genre = Genre.NON_SPECIFIE;
		nbrLivres++;	}

	@Override
	public String toString() {
		return "titre : " + titre + ", auteur : " + auteur + ", editeur : " + editeur + ", nbExemplaires : "
				+ nbExemplaires + ", genre : " + genre;
	}

}
