package Bibliobus;

import java.util.Arrays;

public class Bibliobus {
	String nom;
	
//	Livre [] catalogue = new Livre[100];
	Livre [] catalogue = new Livre[5];
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}

	public Livre[] getCatalogue() {
		return catalogue;
	}

	public void setCatalogue(Livre[] catalogue) {
		this.catalogue = catalogue;
	}

	public Bibliobus(Livre[] catalogue) {
		super();
		this.catalogue = catalogue;
	}

	public Bibliobus() {
		super();
	}
	
	public void ajoutLivre(String titre, String auteur, String editeur){
		Livre livre = new Livre(titre, auteur, editeur);
	
		for (int i = 0; i < catalogue.length; i++) {
			if(catalogue[i] == null) {
				catalogue[i] = livre;
				break;
			}
			if(i == catalogue.length-1) {
				System.out.println("Le catalogue est plein !");
			}
		}
	}
	
	public void supprimerLivre(int id){

		for (int i = id; i < catalogue.length; i++) {
			if (i < catalogue.length-1) {	
				catalogue[i] = catalogue[i + 1];
			}
			else {
				catalogue[i] = null;
			}
		}
		System.out.println("Le livre � �t� supprim� !");
	}
	
	public void afficherCatalogue() {
		System.out.println("Bibliobus '" + getNom() + "'\n");
		System.out.println("Liste des livres");
		for (int i = 0; i < catalogue.length; i++) {
			System.out.println("identifiant : " + i + " " + catalogue[i]);
		}
	}
	
	public void afficherPresentsCatalogue() {
		System.out.println("Bibliobus '" + getNom() + "'\n");
		System.out.println("Liste des livres pr�sents");
		for (int i = 0; i < catalogue.length; i++) {
			if(catalogue[i].estPresent()== true ) {
				System.out.println("identifiant : " + i + " " + catalogue[i]);
			}
		}
	}
	
	public void afficherlivreGenre(Enum<Genre> genre) {

		System.out.println("Liste des livres pour le genre : " + genre);
		for (int i = 0; i < catalogue.length; i++) {
			if(catalogue[i].getGenre() == genre ) {
				System.out.println("identifiant : " + i + " " + catalogue[i]);
			}
		}
	}
	
	public void afficherLivreConnu(Livre livre) {
		Boolean verif = false;
		System.out.println("Recherche du livre");
		
		for (int i = 0; i < catalogue.length; i++) {
			if(livre.equals(catalogue[i])) {
				verif = true;
			}
		}
			
		if (verif == true) {
			System.out.println("Le livre " + livre + " est pr�sent !" );
		} else {
			System.out.println("Le livre " + livre + " n'est pas pr�sent !");
		}
	}
	
	public void indiquerLivrePresentParId(int id) {
		boolean verif = false;
		for (int i = 0; i < catalogue.length; i++) {
			if ((id == i) && (catalogue[i].estPresent() == true)) {
				verif = true;
			}
		}
		
		if (verif == true ) {
			System.out.println("Un livre comprenant cet identifiant est pr�sent !");
		}
		else {
			System.out.println("Aucun livre ne correspond � cet identifiant !");

		}
	}
	
	public void indiquerLivrePresentParTitre(String titre) {
		boolean verif = false;
		for (int i = 0; i < catalogue.length; i++) {
			if (titre == catalogue[i].getTitre()) {
				verif = true;
			}
		}
		
		if (verif == true ) {
			System.out.println("au moins un livre comprenant ce titre est pr�sent !");
		}
		else {
			System.out.println("Aucun livre ne correspond � ce titre !");

		}
	}
	
	public void indiquerLivrePresentParAuteur(String auteur) {
		boolean verif = false;
		for (int i = 0; i < catalogue.length; i++) {
			if (auteur == catalogue[i].getAuteur()) {
				verif = true;
			}
		}
		
		if (verif == true ) {
			System.out.println("Au moins un livre avec cet auteur est pr�sent !");
		}
		else {
			System.out.println("Aucun livre ne correspond � cet auteur !");

		}
	}
	
	public void indiquerLivrePresentParEditeur(String editeur) {
		boolean verif = false;
		for (int i = 0; i < catalogue.length; i++) {
			if (editeur == catalogue[i].getEditeur()) {
				verif = true;
			}
		}
		
		if (verif == true ) {
			System.out.println("Au moins un livre avec cet editeur est pr�sent !");
		}
		else {
			System.out.println("Aucun livre ne correspond � cet auteur !");

		}	
	}
	
	public <T> void indiquerLivrePresent(T entree){
		boolean verif = false;
		for (int i = 0; i < catalogue.length; i++) {
			if ((entree == catalogue[i].getEditeur()) || (entree == catalogue[i].getAuteur()) || (entree == catalogue[i].getTitre())){
				verif = true;
			}		
			else if (((int) entree == i) && (catalogue[(int) entree].estPresent() == true)) {
				verif = true;
			}
			else {
				System.out.println(i + "...");
			}
		}
		
		if (verif == true) {
			System.out.println("Au moins un livre corespond � " + entree + " !");
		}
		else {
			System.out.println("Aucun livre ne correspond � " + entree + " !");
		}
	}
	
	public void indiquerNbExemplairesParId(int id) {
		int  nb = 0;
		for (int i = 0; i < catalogue.length; i++) {
			if ((id == i) && (catalogue[i].estPresent() == true)) {
				nb = catalogue[i].getNbExemplaires();
			}
		}
		
		System.out.println("Il y a " + nb + " exemplaires pr�sents !");


	}
	
	public void indiquerNbExemplairesParTitre(String titre) {
		int  nb = 0;
		for (int i = 0; i < catalogue.length; i++) {
			if (titre == catalogue[i].getTitre()) {
				nb = nb + catalogue[i].getNbExemplaires();
			}
		}
		System.out.println("Il y a " + nb + " exemplaires pr�sents !");
	}
	
	public void indiquerNbExemlpairesParAuteur(String auteur) {
		int  nb = 0;
		for (int i = 0; i < catalogue.length; i++) {
			if (auteur == catalogue[i].getAuteur()) {
				nb = nb + catalogue[i].getNbExemplaires();
			}
		}
		System.out.println("Il y a " + nb + " exemplaires pr�sents !");
	}
	
	public void indiquerNbExemplairesParEditeur(String editeur) {
		int  nb = 0;
		for (int i = 0; i < catalogue.length; i++) {
			if (editeur == catalogue[i].getEditeur()) {
				nb = nb + catalogue[i].getNbExemplaires();
			}
		}
		System.out.println("Il y a " + nb + " exemplaires pr�sents !");
	}
	
	public <T> void indiquerNbExemplaires(T entree) {
		int  nb = 0;
		for (int i = 0; i < catalogue.length; i++) {
			if ((entree == catalogue[i].getTitre()) || (entree == catalogue[i].getAuteur()) || (entree == catalogue[i].getEditeur())) {
				nb = nb + catalogue[i].getNbExemplaires();
			}
			else if (((int)entree == i) && (catalogue[i].estPresent() == true)) {
				nb = nb + catalogue[i].getNbExemplaires();
			}
		}
		System.out.println("Il y a " + nb + " exemplaires pr�sents !");
	}
	
	public void getTitre(int identifiant) {
		System.out.println(catalogue[identifiant].getTitre());
	}
	
	public void getAuteur(int identifiant) {
		System.out.println(catalogue[identifiant].getAuteur());
	}
	
	public void getEditeur(int identifiant) {
		System.out.println(catalogue[identifiant].getEditeur());
	}
	
	public void getGenre(int identifiant) {
		System.out.println(catalogue[identifiant].getGenre());
	}
	
	public void getNbExemplaires(int identifiant) {
		System.out.println(catalogue[identifiant].getNbExemplaires());
	}
	
	public void afficherLivre(int identifiant) {
		System.out.println(catalogue[identifiant]);
	}
	
	public Livre retournerLivre(int identifiant) {
		return catalogue[identifiant];
	}
	
	@Override
	public String toString() {
		return "Bibliobus [bibliobus=" + Arrays.toString(catalogue) + "]";
	}
}
