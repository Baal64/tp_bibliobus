package Bibliobus;

import java.util.*;

public class DemoLivre {

	public static void main(String[] args) {
		
		Livre livre1 = new Livre("Le tour du monde en 80 jours", "J.Vernes", "Folio", 8, Genre.AVENTURE);
		Livre livre2 = new Livre("Rrrrr", "Alain Chabba", "Galimard", 2, Genre.LITTERATURE_JEUNESSE);
		Livre livre3 = new Livre("Tintin, De la Terre � la Lune", "Herg�", "Folio", 8, Genre.BD);
		Livre livre4 = new Livre("Le tour du monde en 80 jours", "J.Vernes", "Folio", 8, Genre.AVENTURE);
		Livre livre5 = new Livre("Ca", "Stephen King", "Folio", 0);
	
		ArrayList<Livre> collectionLivres = new ArrayList<Livre>();
		Collections.addAll(collectionLivres, livre1, livre2, livre3, livre4, livre5);

		
		
// ==============================|| Test getters et setters||=======================================
		
//	System.out.println(livre2.getTitre() + " " + livre2.getAuteur() + " " + livre2.getEditeur() + " " + livre2.getGenre() + " " + livre2.getNbExemplaires());
//	
//	livre2.setGenre(Genre.AVENTURE);
//	livre2.setNbExemplaires(40);
//	
//	System.out.println(livre2.getTitre() + " " + livre2.getAuteur() + " " + livre2.getEditeur() + " " + livre2.getGenre() + " " + livre2.getNbExemplaires());
//	System.out.println(Livre.getNbrLivres());
		
		
		
// ====================|| Test ajout suppression nombre exemplaires ||=========================
		
//		System.out.println(livre1.getNbExemplaires());
//		livre1.nouvelExemplaire();
//		System.out.println(livre1.getNbExemplaires());
//		livre1.nouvelExemplaire(5);
//		System.out.println(livre1.getNbExemplaires());
//		livre1.perteExemplaire();
//		System.out.println(livre1.getNbExemplaires());
//	
		
// =====================|| tests Nouvel editeur ||==========================		
		
//		System.out.println(livre1);
//		String editeur = "Jeunesse";
//		
//		livre1.nouvelEditeur(editeur);
//		System.out.println(livre1);
	
		
// =================|| tests Exemplaires identiques  ||=====================

//	System.out.println(livre1.equals(livre4));

		
// =====================|| tests toString ||===============================
		
//		System.out.println(livre1.toString());
		
		
// =====================|| tests estPresent ||===============================

//		System.out.println(livre5.estPresent());
		
		
		
		
		
		
	}

}
