package Bibliobus;

public enum Genre {
	LITTERATURE_FRANCAISE("Litteratre francaise", 1),
	LITTERATURE_JEUNESSE("Litterature jeunesse", 2),
	LITTERATURE_ETRANGERE("Romans", 3),
	POLICIER("Policier", 4),
	SCIENCE_FICTION("Science fiction", 5),
	FANTASTIQUE("Fantastique", 6),
	HORREUR("Horreur", 7),
	AVENTURE("Aventure", 8),
	BD("BD", 9),
	NON_SPECIFIE("Non sp�cifi�", 10);
	
	private final String genre;
	private final int code;
	
	public String getGenre() {
		return genre;
	}
	public int getCode() {
		return code;
	}
	private Genre(String genre, int code) {
		this.genre = genre;
		this.code = code;
	}
		
}
