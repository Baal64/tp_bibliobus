package Bibliobus;

public class DemoBibliobus {

	public static void main(String[] args) {
		
		Bibliobus bibliobus = new Bibliobus();
		bibliobus.setNom("Totoro");
				
// ==============================|| ajout livre ||=======================================
// Limitation du tableau de 100 � 5 pour le test
		
		bibliobus.ajoutLivre("La route", "Cormac McCarthy", "�ditions de l'Olivier");
		bibliobus.ajoutLivre("La route2", "Cormac McCarthy", "�ditions de l'Olivier");
		bibliobus.ajoutLivre("La route3", "Cormac McCarthy", "�ditions de l'Olivier");
		bibliobus.ajoutLivre("La route4", "Cormac McCarthy", "�ditions de l'Olivier");
		bibliobus.ajoutLivre("La route5", "Cormac McCarthy", "�ditions de l'Olivier");
//		bibliobus.ajoutLivre("La route6", "Cormac McCarthy", "�ditions de l'Olivier");
		
		
		

// ==============================|| afficher catalogue ||================================
		
//		bibliobus.afficherCatalogue();
		
		
// ==============================|| Methodes get by id ||================================

//		bibliobus.getEditeur(1);
//		bibliobus.getAuteur(1);
//		bibliobus.getGenre(1);
//		bibliobus.getTitre(1);
//		bibliobus.getNbExemplaires(1);
		
		
// ================================|| afficher livre ||==================================

//		bibliobus.afficherLivre(1);
	

// ================================|| retourner livre ||==================================

//		System.out.println(bibliobus.retournerLivre(1));

		
// ===========================|| afficher  livres  pr�sents ||============================
	
		bibliobus.catalogue[0].setNbExemplaires(20);
		bibliobus.catalogue[1].setNbExemplaires(15);
		bibliobus.catalogue[2].setNbExemplaires(17);
		bibliobus.catalogue[3].setNbExemplaires(4);
//
//		bibliobus.afficherPresentsCatalogue();
//		
// ===========================|| afficher  livres  par genre ||===========================

//		
		bibliobus.catalogue[0].setGenre(Genre.LITTERATURE_ETRANGERE);
		bibliobus.catalogue[1].setGenre(Genre.SCIENCE_FICTION);
		bibliobus.catalogue[2].setGenre(Genre.LITTERATURE_ETRANGERE);
		bibliobus.catalogue[3].setGenre(Genre.SCIENCE_FICTION);
//	
//		bibliobus.afficherlivreGenre(Genre.NON_SPECIFIE);
		
		

// =============================|| Chercher si livre connu ||=============================
//
//		Livre livre = new Livre("La routlmjkhme", "Cormac McCarthy", "�ditions de l'Olivier");
//		bibliobus.afficherLivreConnu(livre);

	
// ==============|| Chercher si livre pr�sent par id, titre, auteur, editeur ||============
	
//	bibliobus.indiquerLivrePresentParId(1);
//	bibliobus.indiquerLivrePresentParTitre("La route");
//	bibliobus.indiquerLivrePresentParAuteur("Cormac McCarthy");
//	bibliobus.indiquerLivrePresentParEditeur("�ditions de l'Olivier");
		
// ==============|| Test methode generique ||================
		
	bibliobus.indiquerLivrePresent(1);
//	bibliobus.indiquerLivrePresent("La route2");
	bibliobus.indiquerLivrePresent("Cormac McCarthy");
	bibliobus.indiquerLivrePresent("�ditions de l'Olivier");
//	bibliobus.indiquerLivrePresent("mljhmljm");
	bibliobus.indiquerLivrePresent(100);

		
// ==============|| Chercher nombre d'exemplaires par id, titre, auteur, editeur ||========
		
//	bibliobus.indiquerNbExemplairesParId(1);
//	bibliobus.indiquerNbExemplairesParTitre("La route");
//	bibliobus.indiquerNbExemlpairesParAuteur("Cormac McCarthy");
//	bibliobus.indiquerNbExemplairesParEditeur("�ditions de l'Olivier");
	
// ==============|| Test methode generique ||================
	
//	bibliobus.indiquerNbExemplaires("La route");
	bibliobus.indiquerNbExemplaires("Cormac McCarthy");
	bibliobus.indiquerNbExemplaires("�ditions de l'Olivier");
//	bibliobus.indiquerNbExemplaires("gdgqdhqdh");
	bibliobus.indiquerNbExemplaires(100);

		
// =================================|| Supprimer un livre ||===============================
//		bibliobus.afficherCatalogue();
//		System.out.println();
//		bibliobus.supprimerLivre(0);
//		bibliobus.afficherCatalogue();
//		bibliobus.supprimerLivre(2);
//		bibliobus.afficherCatalogue();
	}
}
